<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or congit pushtroller method. Build something great!
|
*/

Route::group(['prefix' => 'Blog'], function () {
    Route::get('/', function () {
        dd('This is the Blog module index page. Build something great!');

    });

    Route::get('/dashboard','ManageController@index');
});
