<?php

namespace App\Modules\Blog\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('Blog', 'Resources/Lang', 'app'), 'Blog');
        $this->loadViewsFrom(module_path('Blog', 'Resources/Views', 'app'), 'Blog');
        $this->loadMigrationsFrom(module_path('Blog', 'Database/Migrations', 'app'), 'Blog');
        $this->loadConfigsFrom(module_path('Blog', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('Blog', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
